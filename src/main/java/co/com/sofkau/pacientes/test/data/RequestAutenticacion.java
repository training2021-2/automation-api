package co.com.sofkau.pacientes.test.data;

import co.com.sofkau.pacientes.test.model.JsonAutenticacion;

public class RequestAutenticacion {

    public static JsonAutenticacion getValidUser() {
        return JsonAutenticacion.builder()
                .username("Covid")
                .password("Covid")
                .build();
    }

    public static JsonAutenticacion getInvalidPassword() {
        return JsonAutenticacion.builder()
                .username("Covid")
                .password("YUFyf7e6")
                .build();
    }
}
