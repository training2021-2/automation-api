#language: es

Característica: Autenticación de usuario en la api

  Escenario: Autenticacion exitosa
    Cuando un "usuario" tiene credenciales validas
    Entonces se puede autenticar en la api correctamente

  Escenario: Autenticacion fallida
    Cuando un "usuario" tiene credenciales incorrectas
    Entonces la api contesta con codigo de error "403"