package screenplay.steps;

import co.com.sofkau.pacientes.test.data.RequestAutenticacion;
import co.com.sofkau.pacientes.test.task.project.TokenRequest;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;

import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.startsWith;

public class Autenticacion {

    private Actor actor;

    @Cuando("un {string} tiene credenciales validas")
    public void unTieneCredencialesValidas(String name) {
        actor = Actor.named(name);
        actor.attemptsTo(TokenRequest.executed(RequestAutenticacion.getValidUser()));
    }

    @Entonces("se puede autenticar en la api correctamente")
    public void sePuedeAutenticarEnLaApiCorrectamente() {
        actor.should(
                seeThatResponse("La api entrego código 200 correctamente",
                        response -> response.statusCode(200)
                )
        );
        actor.should(
                seeThatResponse("Se genero el token correctamente",
                        response -> response.body(startsWith("{\"jwt\":\"ey")))
        );
    }

    @Cuando("un {string} tiene credenciales incorrectas")
    public void unTieneCredencialesIncorrectas(String arg0) {
        actor = Actor.named(arg0);
        actor.attemptsTo(TokenRequest.executed(RequestAutenticacion.getInvalidPassword()));
    }

    @Entonces("la api contesta con codigo de error {string}")
    public void laApiContestaConCodigoDeError(String codigo) {
        actor.should(
                seeThatResponse("Se entrego el código de respuesta esperado",
                        response -> response.statusCode(Integer.parseInt(codigo))
                )
        );
    }
}
